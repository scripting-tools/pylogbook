import enum


class NamedServer(enum.Enum):
    """Named logbook service servers."""
    #: The test logbook service.
    TEST = "https://cs-ccr-logdev.cern.ch/elogbook-server"

    #: The production logbook service.
    PRO = "https://logbook.cern.ch/elogbook-server"
