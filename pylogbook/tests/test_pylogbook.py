"""
High-level tests for the pylogbook package.

"""
import datetime
import typing

import pylogbook
import pylogbook.models


def test_version() -> None:
    assert pylogbook.__version__ is not None


NO_ATTACHMENT: dict = {
    "logbookID": None,
    "comment": "Testing from pylogbook v3.",
    "creator": "pelson  @lxplus744.cern.ch (2020/12/04 13:03:10)",
    "dateOfEvent": "2020-12-04T13:03:10",
    "tags": [],
    "activities": [],
    "attachments": None,
    "childrenIds": [],
    "id": 2729882,
    "url": "/GET/event/2729882",
}

WITH_ATTACHMENT = {
    "activities": [
        {
            "creator": "importer",
            "dateEnd": "2019-04-26T16:50:26",
            "dateStart": "2019-03-26T09:56:31",
            "id": 2,
            "name": "LOGBOOK_TESTS",
            "tags": [],
            "url": "/GET/activity/2",
        },
    ],
    "attachments": [
        {
            "creator": "pelson  @lxplus744.cern.ch (2020/12/04 13:57:57)",
            "fileName": "snake.png",
            "id": 3224,
            "preview": {
                "creator": "pelson  @lxplus744.cern.ch (2020/12/04 13:57:57)",
                "description": "Preview of snake.png",
                "fileName": "preview_snake.png",
                "id": 3225,
                "tags": [],
                "type": {
                    "id": 99,
                    "name": "image/png",
                    "url": "/GET/mime/99",
                    "urlToIcon": "/GET/mime/99/icon",
                },
                "url": "/GET/attachment/3225",
                "urlToContent": "/GET/attachment/3225/content",
            },
            "tags": [],
            "type": {
                "id": 99,
                "name": "image/png",
                "url": "/GET/mime/99",
                "urlToIcon": "/GET/mime/99/icon",
            },
            "url": "/GET/attachment/3224",
            "urlToContent": "/GET/attachment/3224/content",
        },
        {
            "creator": "pelson  @lxplus744.cern.ch (2020/12/04 13:57:57)",
            "fileName": "my-file.txt",
            "id": 3226,
            "tags": [],
            "type": {
                "id": 215,
                "name": "application/octet-stream",
                "url": "/GET/mime/215",
                "urlToIcon": "/GET/mime/215/icon",
            },
            "url": "/GET/attachment/3226",
            "urlToContent": "/GET/attachment/3226/content",
        },
    ],
    "childrenIds": [],
    "comment": "Testing from pylogbook v3.",
    "creator": "pelson  @lxplus744.cern.ch (2020/12/04 13:57:57)",
    "dateOfEvent": "2020-12-04T13:57:57",
    "id": 2729886,
    "tags": [{"name": "AccessRequest", "creator": "...", "id": 11105, "url": "..."}],
    "url": "/GET/event/2729886",
}


def test_event() -> None:
    client = typing.cast(pylogbook._client.LogbookServiceClient, None)
    event = pylogbook.models.Event.build_from_json_response(client, NO_ATTACHMENT)

    assert event.date == datetime.datetime(2020, 12, 4, 13, 3, 10)


def test_event_w_attachment() -> None:
    client = typing.cast(pylogbook._client.LogbookServiceClient, None)
    event = pylogbook.models.Event.build_from_json_response(client, WITH_ATTACHMENT)
    assert isinstance(event.attachments[0], pylogbook.models.Attachment)


def test_event_w_tags() -> None:
    client = typing.cast(pylogbook._client.LogbookServiceClient, None)
    event = pylogbook.models.Event.build_from_json_response(client, WITH_ATTACHMENT)
    assert isinstance(event.tags[0], pylogbook.models.Tag)
