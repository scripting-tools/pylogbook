from __future__ import annotations

from dataclasses import dataclass
import json
from pathlib import Path
import re
import typing
import unittest.mock

import pytest

import pylogbook
from pylogbook._attachment_builder import AttachmentBuilder
from pylogbook._low_level_client import LogbookServiceClient
import pylogbook.auth
import pylogbook.models

TESTS_LOGBOOK_NAME = 'LOGBOOK_TESTS_Long_Name Testing'


if typing.TYPE_CHECKING:
    class MockedLogbookServiceClient(LogbookServiceClient, unittest.mock.Mock):
        pass


@pytest.fixture
def mocked_ll_client() -> typing.Generator[MockedLogbookServiceClient, None, None]:
    patch = unittest.mock.patch('pylogbook._client.LogbookServiceClient', autospec=True)

    with patch as patched_obj:
        yield patched_obj


@dataclass
class SimpleResponse:
    status_code: int
    content: bytes

    def json(self):
        return json.loads(self.content)


def read_sample(name) -> bytes:
    here = Path(__file__).absolute().parent
    fname = here / 'samples' / (name + '.resp')
    return fname.read_bytes()


@pytest.fixture
def sample_activity(mocked_ll_client: MockedLogbookServiceClient, mock_get_logbooks):
    return pylogbook.Client().get_activities()[TESTS_LOGBOOK_NAME]


@pytest.fixture
def post_event_mocked(mocked_ll_client):
    mocked_ll_client.return_value.post_event.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('post_event'),
    )
    return mocked_ll_client.return_value.post_event


def test_add_event(mocked_ll_client: MockedLogbookServiceClient, post_event_mocked, sample_activity) -> None:
    logbook = pylogbook.ActivitiesClient(sample_activity.name)
    event = logbook.add_event(message='123')
    post_event_mocked.assert_called_once_with(
        '123', activities=(sample_activity.activity_id,),
        tags=(),
    )
    assert isinstance(event, pylogbook.models.Event)


def test_add_event__no_auth(mocked_ll_client: MockedLogbookServiceClient, sample_activity) -> None:
    mocked_ll_client.return_value.post_event.return_value = SimpleResponse(
        status_code=401,
        content=read_sample('post_event'),
    )
    cli = pylogbook.Client()
    with pytest.raises(pylogbook.exceptions.AuthenticationError):
        cli.add_event(sample_activity, message='123')


def test_add_event_single_activity_instance(
        mocked_ll_client: MockedLogbookServiceClient, post_event_mocked,
        sample_activity,
) -> None:
    logbook = pylogbook.ActivitiesClient(sample_activity)
    event = logbook.add_event(message='123')
    post_event_mocked.assert_called_once_with(
        '123', activities=(sample_activity.activity_id,), tags=(),
    )
    assert isinstance(event, pylogbook.models.Event)


def test_add_event_multiple_activity_instance(
        mocked_ll_client: MockedLogbookServiceClient, post_event_mocked, mock_get_logbooks,
) -> None:
    activity = pylogbook.Client().get_activities()[TESTS_LOGBOOK_NAME]
    logbook = pylogbook.ActivitiesClient([activity, activity])
    event = logbook.add_event(message='123')
    post_event_mocked.assert_called_once_with(
        '123', activities=(activity.activity_id, activity.activity_id),
        tags=(),
    )
    assert isinstance(event, pylogbook.models.Event)


def test_add_event__with_tag(
        mocked_ll_client: MockedLogbookServiceClient,
        post_event_mocked,
        mock_get_logbooks,
        mocked_tags,
) -> None:
    cli = pylogbook.Client()
    activity = cli.get_activities()[TESTS_LOGBOOK_NAME]
    tags = iter(cli.get_tags())
    tag1, tag2 = next(tags), next(tags)
    logbook = pylogbook.ActivitiesClient(activity)

    # Resetup tags responses for tag expansion in add_event.
    mocked_tags_factory(mocked_ll_client)
    event = logbook.add_event(message='123', tags=[tag1, tag2.name])
    post_event_mocked.assert_called_once_with(
        '123', activities=(activity.activity_id,),
        tags=(tag1.tag_id, tag2.tag_id),
    )
    assert isinstance(event, pylogbook.models.Event)


def test_activities_client_add_activity(
        mocked_ll_client: MockedLogbookServiceClient, post_event_mocked, mock_get_logbooks,
) -> None:
    activity = pylogbook.Client().get_activities()[TESTS_LOGBOOK_NAME]
    logbook = pylogbook.ActivitiesClient([activity, activity])
    assert logbook.activities == (activity, activity)
    logbook.activities = (activity,)
    assert logbook.activities == (activity,)

    logbook.activities = activity  # type: ignore  # reason: https://github.com/python/mypy/issues/3004
    assert logbook.activities == (activity,)

    message = re.escape(
        "No activity with id -1 was found on the server.",
    )
    with pytest.raises(ValueError, match=message):
        logbook.activities = [-1, -123, activity, activity, activity.activity_id]   # type: ignore

    message = re.escape(
        'No activity named "doesn\'t exist" was found on the server.',
    )
    with pytest.raises(ValueError, match=message):
        logbook.activities = ["doesn't exist"]  # type: ignore  # reason: https://github.com/python/mypy/issues/3004


def test_get_event(mocked_ll_client: MockedLogbookServiceClient) -> None:
    mocked_ll_client.return_value.get_event.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('get_event'),
    )
    client = pylogbook.Client()
    event = client.get_event(123)
    mocked_ll_client.return_value.get_event.assert_called_once_with(123)
    assert isinstance(event, pylogbook.models.Event)


def test_get_event__no_auth(mocked_ll_client: MockedLogbookServiceClient) -> None:
    mocked_ll_client.return_value.get_event.return_value = SimpleResponse(
        status_code=401,
        content=read_sample('get_event'),
    )
    client = pylogbook.Client()
    with pytest.raises(pylogbook.exceptions.AuthenticationError):
        client.get_event(123)


def test_get_events(mocked_ll_client: MockedLogbookServiceClient) -> None:
    mocked_ll_client.return_value.get_events.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('get_events.count_only'),
    )
    client = pylogbook.Client()
    events = client.get_events()
    mocked_ll_client.return_value.get_events.assert_called_once_with(
        count_only=True, activities=None, from_date=None, to_date=None, tags=None,
    )
    mocked_ll_client.return_value.get_events.reset_mock()
    assert isinstance(events, pylogbook.models.PaginatedEvents)
    assert events.count == 1925983  # The value in get_events.count_only.resp
    assert events.page_size == 25
    assert events.count <= events.n_pages() * events.page_size < events.count + events.page_size

    mocked_ll_client.return_value.get_events.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('get_events'),
    )
    event = next(iter(events))
    assert isinstance(event, pylogbook.models.Event)
    mocked_ll_client.return_value.get_events.assert_called_once_with(
        records_limit=25, start_record=0, activities=None, from_date=None,
        to_date=None, count_only=False, tags=None,
    )

    mocked_ll_client.return_value.get_events.return_value = SimpleResponse(
        status_code=401,
        content=read_sample('get_events.count_only'),
    )
    with pytest.raises(pylogbook.exceptions.AuthenticationError):
        next(iter(events))


def test_get_events__no_auth(mocked_ll_client: MockedLogbookServiceClient) -> None:
    mocked_ll_client.return_value.get_events.return_value = SimpleResponse(
        status_code=401,
        content=read_sample('get_events.count_only'),
    )
    client = pylogbook.Client()
    with pytest.raises(pylogbook.exceptions.AuthenticationError):
        client.get_events()


@pytest.fixture
def sample_event(mocked_ll_client: MockedLogbookServiceClient, mock_get_logbooks):
    mocked_ll_client.return_value.post_event.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('post_event'),
    )
    logbook = pylogbook.ActivitiesClient(
        pylogbook.Client().get_activities()[TESTS_LOGBOOK_NAME],
    )
    return logbook.add_event(message='123')


def test_get_event__from_event(mocked_ll_client: MockedLogbookServiceClient, sample_event) -> None:
    mocked_ll_client.return_value.get_event.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('get_event'),
    )
    client = pylogbook.Client()
    event = client.get_event(sample_event)
    mocked_ll_client.return_value.get_event.assert_called_once_with(sample_event.event_id)
    assert isinstance(event, pylogbook.models.Event)


def test_event__attach_file(mocked_ll_client: MockedLogbookServiceClient, sample_event, tmp_path) -> None:
    mocked_ll_client.return_value.add_attachment.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('add_attachment.from_file'),
    )
    file_to_attach = Path(tmp_path) / 'example-file.txt'
    with file_to_attach.open('wt') as fh:
        fh.write('test file contents')
    attachment = sample_event.attach_file(file_to_attach)
    mocked_ll_client.return_value.add_attachment.assert_called_once_with(
        sample_event.event_id, AttachmentBuilder(
            contents=b'test file contents', short_name=file_to_attach.name, mime_type='text/plain',
        ),
    )
    assert isinstance(attachment, pylogbook.models.Attachment)


def test_event__attach_content(mocked_ll_client: MockedLogbookServiceClient, sample_event) -> None:
    mocked_ll_client.return_value.add_attachment.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('add_attachment.from_string'),
    )
    attachment = sample_event.attach_content('test contents')
    mocked_ll_client.return_value.add_attachment.assert_called_once_with(
        sample_event.event_id, AttachmentBuilder(
            contents='test contents', short_name='file', mime_type='text/plain',
        ),
    )
    assert isinstance(attachment, pylogbook.models.Attachment)


def test_event__attach_content_bytes(mocked_ll_client: MockedLogbookServiceClient, sample_event) -> None:
    mocked_ll_client.return_value.add_attachment.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('add_attachment.from_string'),
    )
    attachment = sample_event.attach_content(
        b'test contents', name='a_picture.png', mime_type='image/png',
    )
    mocked_ll_client.return_value.add_attachment.assert_called_once_with(
        sample_event.event_id, AttachmentBuilder(
            contents=b'test contents', short_name='a_picture.png', mime_type='image/png',
        ),
    )
    assert isinstance(attachment, pylogbook.models.Attachment)


def mocked_tags_factory(mocked_ll_client: MockedLogbookServiceClient, repeats=1) -> None:
    # A simple setup that mocks all the calls required for get_tags/get_tag.
    # The result will effectively be several pages of the same response
    # (get_tags.page1.resp)
    mocked_ll_client.return_value.get_tags.side_effect = (
        [
            SimpleResponse(
                status_code=200,
                content=read_sample('get_tags.count'),
            ),
        ] +
        [
            SimpleResponse(
                status_code=200,
                content=read_sample('get_tags.page1'),
            ),
        ] * 3
    ) * repeats
    return


mocked_tags = pytest.fixture(mocked_tags_factory)


def test_get_tag__found(mocked_tags) -> None:
    client = pylogbook.Client()
    tag = client.get_tag(name='LN3OP')  # As seen in get_tags.page1.resp document.
    assert isinstance(tag, pylogbook.models.Tag)


def test_get_tag__not_found(mocked_tags) -> None:
    client = pylogbook.Client()
    with pytest.raises(ValueError, match=r'The tag named "not-in-the-list" was not found'):
        client.get_tag(name='not-in-the-list')


def test_get_tags(mocked_tags, mocked_ll_client) -> None:
    client = pylogbook.Client()
    tags = client.get_tags()
    mocked_ll_client.return_value.get_tags.assert_called_once_with(count_only=True)
    mocked_ll_client.return_value.get_tags.reset_mock()
    assert isinstance(tags, pylogbook.models.PaginatedTags)

    assert tags.count == 2617  # The value in get_tags.count.resp
    assert tags.count <= tags.page_size * tags.n_pages() < tags.count + tags.page_size

    assert tags.n_pages() == 3
    tag = list(tags)
    assert isinstance(tag[0], pylogbook.models.Tag)
    calls = (
        unittest.mock.call(count_only=False, records_limit=1000, start_record=0),
        unittest.mock.call(count_only=False, records_limit=1000, start_record=1000),
        unittest.mock.call(count_only=False, records_limit=1000, start_record=2000),
    )
    mocked_ll_client.return_value.get_tags.assert_has_calls(calls)


def test_get_tags__no_auth(mocked_ll_client) -> None:
    mocked_ll_client.return_value.get_tags.side_effect = [
        SimpleResponse(
            status_code=200,
            content=read_sample('get_tags.count'),
        ),
        SimpleResponse(
            status_code=401,
            content=read_sample('get_tags.count'),
        ),
    ]
    client = pylogbook.Client()
    tags = client.get_tags()
    with pytest.raises(pylogbook.exceptions.AuthenticationError):
        list(tags.get_page(1))


@pytest.fixture
def mock_get_logbooks(mocked_ll_client):
    mocked_ll_client.return_value.get_logbooks.return_value = SimpleResponse(
        status_code=200,
        content=read_sample('get_logbooks'),
    )
    return mocked_ll_client.return_value.get_logbooks


def test_get_activities(mock_get_logbooks) -> None:
    logbook = pylogbook.Client()
    activities1 = logbook.get_activities()
    assert isinstance(activities1, dict)
    activities2 = list(activities1.values())
    assert isinstance(activities2[0], pylogbook.models.Activity)
    result = [(activity.activity_id, activity.name) for activity in activities2]
    assert result[:3] == [(2, TESTS_LOGBOOK_NAME), (79, 'LINAC_4'), (1, 'TOF')]


def test_client__rbac_token() -> None:
    client = pylogbook.Client(rbac_token='foo')
    assert client.rbac_b64_token == 'foo'
    client.rbac_b64_token = 'something else'
    assert client.rbac_b64_token == 'something else'
    assert client._service_client.rbac_b64_token == 'something else'


def test_client__rbac_token_not_found(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.delenv("RBAC_TOKEN_SERIALIZED", raising=False)
    with pytest.raises(ValueError, match='Unable to get the RBAC token'):
        pylogbook.Client()


def test_client__rbac_token_by_default(monkeypatch: pytest.MonkeyPatch) -> None:
    # If no token is specified we must still use RBAC by default. This allows
    # things like the special RBAC environment variable to be considered, and
    # allows users to update the RBAC token after first initialisation (a
    # use-case that needs to be maintained).
    monkeypatch.setenv("RBAC_TOKEN_SERIALIZED", "Fake")
    client = pylogbook.Client()
    assert isinstance(client.auth, pylogbook.auth.RbacAuthManager)
    assert client.rbac_b64_token == 'Fake'


def test_client__multiple_auth_error() -> None:
    with pytest.raises(TypeError, match='Specific authentication and RBAC tokens are mutually exclusive'):
        pylogbook.Client(rbac_token='foo', auth=pylogbook.auth.AuthManager())


def test_client__bearer_auth() -> None:
    client = pylogbook.Client(auth=pylogbook.auth.BearerToken('some-token'))
    assert client.auth.http_headers() == {'Authorization': 'Bearer some-token'}
