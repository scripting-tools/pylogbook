"""
This script is used to build the sample outputs. It will write to the
LOGBOOK_TESTS logbook (https://cs-ccr-logdev.cern.ch/elogbook-server#/logbook?logbookId=2).

"""

import json
from pathlib import Path

import pylogbook
from pylogbook._attachment_builder import AttachmentBuilder
from pylogbook._low_level_client import LogbookServiceClient

HERE = Path(__file__).absolute().parent


def format_json(content: bytes) -> str:
    return json.dumps(json.loads(content), indent=4, sort_keys=True) + '\n'


server = pylogbook.NamedServer.TEST
cli = LogbookServiceClient(
    server_url=server,
)
high_level_cli = pylogbook.Client(server_url=server)


resp = cli.get_tags(count_only=True)
with (HERE / 'get_tags.count.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


resp = cli.get_tags(start_record=0, records_limit=10)
with (HERE / 'get_tags.page1.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


resp = cli.get_logbooks()
with (HERE / 'get_logbooks.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


logbook_tests = high_level_cli.get_activities()['LOGBOOK_TESTS_Long_Name Testing']
resp = cli.post_event(
    message="A test event",
    activities=(logbook_tests.activity_id,),
    tags=(high_level_cli.get_tag(name='TESTS').tag_id,),
)
with (HERE / 'post_event.resp').open('wt') as fh:
    fh.write(format_json(resp.content))
event_id = resp.json()['id']


resp = cli.add_attachment(
    event_id, AttachmentBuilder.from_string('attach_file'),
)
with (HERE / 'add_attachment.from_string.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


resp = cli.add_attachment(
    event_id, AttachmentBuilder.from_file(HERE / 'add_attachment.from_string.resp'),
)
with (HERE / 'add_attachment.from_file.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


resp = cli.get_event(event_id)
with (HERE / 'get_event.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


resp = cli.delete_event(event_id)
with (HERE / 'delete_event.resp').open('wt') as fh:
    assert resp.status_code == 401  # Shouldn't be raising, but is currently.
    fh.write(resp.content.decode('utf-8'))
    # NOTE: Response is not currently JSON...
    # fh.write(format_json(resp.content))


resp = cli.get_events(count_only=True)
with (HERE / 'get_events.count_only.resp').open('wt') as fh:
    fh.write(format_json(resp.content))


resp = cli.get_events()
with (HERE / 'get_events.resp').open('wt') as fh:
    fh.write(format_json(resp.content))
