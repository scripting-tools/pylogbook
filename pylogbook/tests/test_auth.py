import base64

import pytest

import pylogbook.auth as auth


def test_pyrbac_v0_like() -> None:
    class PyRBACToken:
        def get_encoded(self) -> bytes:
            return b'the encoded token'

        def get_user_name(self) -> str:
            return 'testing'

    with pytest.warns(
            DeprecationWarning,
            match="pyrbac v0.x support in pylogbook will be removed "
                  "from version 3.6, consider upgrading to pyrbac v1",
    ):
        mgr = auth.RbacAuthManager(b64_token=PyRBACToken())
    expected = base64.b64encode(b'the encoded token').decode()
    assert mgr.b64_token == expected
    assert mgr.http_headers() == {'Authorization': f'RBAC {expected}'}


def test_pyrbac_v1_like() -> None:
    class PyRBACToken:
        def encode(self) -> bytes:
            return b'the encoded token'

        @property
        def user_name(self) -> str:
            return 'testing'

    mgr = auth.RbacAuthManager(b64_token=PyRBACToken())
    expected = base64.b64encode(b'the encoded token').decode()
    assert mgr.b64_token == expected
    assert mgr.http_headers() == {'Authorization': f'RBAC {expected}'}


def test_rbac_token_none(monkeypatch: pytest.MonkeyPatch) -> None:
    # The RBAC token is not allowed to be None.
    monkeypatch.delenv("RBAC_TOKEN_SERIALIZED", raising=False)

    with pytest.raises(ValueError, match='Unable to get the RBAC token'):
        auth.RbacAuthManager()

    # We support passing an empty token, as this is a use-case which is valuable
    # when it is not known up-front what the token is. (as requested by T. Levens)
    a = auth.RbacAuthManager(b64_token='')
    assert a.b64_token == ''
