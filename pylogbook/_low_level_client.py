from __future__ import annotations

import datetime
import json
import logging
import typing
import warnings

import requests
from urllib3.exceptions import InsecureRequestWarning

# Note: no imports from .models should be here - the models build on top of the
# low-level client in this module.
from . import auth as _auth
from ._attachment_builder import AttachmentBuilder
from ._server_names import NamedServer

logger = logging.getLogger(__name__)


def format_date(date: datetime.datetime) -> str:
    return date.isoformat("T", "seconds")


class LogbookServiceClient:
    """A client for interacting directly (via REST) with the logbook service

    This should not implement richer behaviour than is defined in the OpenAPI/SWAGGER
    documentation at https://cs-ccr-logdev.cern.ch/elogbook-server/#/api.

    Note that all methods returning responses may be error responses (e.g. 4xx).
    It is the responsibility of the user of this LowLevelClient to validate the
    response codes appropriately.

    """

    def __init__(
            self,
            server_url: typing.Union[str, NamedServer] = NamedServer.PRO,
            *,
            auth: typing.Optional[_auth.AuthManager] = None,
            rbac_token: typing.Optional[_auth.TokenType] = None,
    ) -> None:
        """
        Builds an authorised (via RBAC token) LogbookServiceClient client, which may be used to
        interact with the OP logbook service.

        Parameters
        ----------
        server_url:
            address of the server
        rbac_token:
            RBAC token, if not provided it will try to take it from the environment
        auth:
            the authentication scheme to use against the Logbook service.
        """
        logger.debug("Creating LogbookServiceClient object: %s", server_url)
        if isinstance(server_url, NamedServer):
            server_url = server_url.value

        if rbac_token is not None and auth is not None:
            raise TypeError('Specific authentication and RBAC tokens are mutually exclusive')
        elif auth is not None:
            self._auth = auth
        else:
            # Important note: We fall back to Rbac authentication by default. This means that
            # a token in the special RBAC token environment variable gets considered.
            self._auth = _auth.RbacAuthManager(rbac_token)
        self._server_url = server_url

    @property
    def server_url(self) -> str:
        return self._server_url

    @property
    def auth(self) -> _auth.AuthManager:
        """Return the Authentication manager for this client"""
        return self._auth

    @property
    def rbac_b64_token(self) -> str:
        """
        If the RBAC authentication scheme is in use, return the RBAC token that is
        set on the :attr:`.auth`` manager.

        If the RBAC authentication scheme is not in use, a TypeError will be raised.
        """
        if not isinstance(self._auth, _auth.RbacAuthManager):
            raise TypeError('Cannot get the RBAC token for a non RBAC authenticated client')
        return self._auth.b64_token

    @rbac_b64_token.setter
    def rbac_b64_token(self, token: _auth.TokenType) -> None:
        if not isinstance(self._auth, _auth.RbacAuthManager):
            raise TypeError('Cannot set the RBAC token for a non RBAC authenticated client')
        # Type ignored due to https://github.com/python/mypy/issues/3004
        self._auth.b64_token = token  # type: ignore

    @staticmethod
    def all_request_ok(requests_: typing.List[requests.Response]) -> bool:
        """
        :param requests_: list of requests
        :return: all requests are okay
        """
        return all(map(lambda r: r.ok, requests_))

    def _make_authd_request(self, method, url, **kwargs) -> requests.Response:

        kwargs.setdefault('headers', {}).update(self._auth.http_headers())
        kwargs.setdefault("verify", False)

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=InsecureRequestWarning)
            return requests.request(method, url, **kwargs)

    def post_event(
            self,
            message: str,
            activities: typing.Tuple[int, ...],
            tags: typing.Tuple[int, ...] = (),
    ) -> requests.Response:
        """
        Post an event (message) to the logbook

        :param message: message to be posted
        :param activities: tuple of activity ids
        :param tags: tuple of tag ids (empty by default)
        :return: results of the request [requests.Response]
        """
        logger.debug("Posting message to activities %s", activities)

        activity_list = [{"id": activity} for activity in activities]
        payload = {
            "dateOfEvent": format_date(datetime.datetime.now()),
            "activities": activity_list,
            "comment": message,
            "tags": [{"id": tag} for tag in tags],
        }

        headers = {"Content-Type": "application/json"}

        url = self._server_url + "/POST/event"

        response = self._make_authd_request(
            'post', url, json=payload, headers=headers,
        )
        logger.debug("POST/event request response: %d", response.status_code)
        return response

    def add_attachment(self, event_id: int, attachment: AttachmentBuilder) -> requests.Response:
        """
        Add an attachment to an already existing event

        :param event_id: id of the event
        :param attachment: attachment to be added
        :return: result of the request [requests.Response]
        """
        logger.debug("Adding attachment %s to event %d", attachment.short_name, event_id)
        url = self._server_url + "/POST/attachment"

        multipart = {
            'attachment': (
                "blob", json.dumps({
                    'fileName': attachment.short_name,
                    'description': '',
                    'tags': [],
                }), "application/json",
            ),
            'content': ("blob", attachment.contents, attachment.mime_type),
            'eventId': ("blob", event_id),
        }

        response = self._make_authd_request('post', url, files=multipart)
        logger.debug("POST/attachment request response: %s", response.status_code)
        return response

    def get_event(self, event_id: int) -> requests.Response:
        """
        Get event with the specific event_id

        :param event_id: event_id to be fetched
        :return: request with the contents of the event
        """
        logger.debug("Retrieving event %d", event_id)
        url = self._server_url + f"/GET/events/{event_id}"
        response = self._make_authd_request('get', url)
        logger.debug("GET/events/%d request response: %d", event_id, response.status_code)
        return response

    def get_events(
            self,
            activities: typing.Optional[typing.Tuple[int, ...]] = None,
            count_only: bool = False,
            records_limit: int = 10,
            start_record: int = 0,
            from_date: typing.Optional[datetime.datetime] = None,
            to_date: typing.Optional[datetime.datetime] = None,
            tags: typing.Optional[typing.Sequence[str]] = None,
    ) -> requests.Response:
        """Get all events matching the given criteria."""
        logger.debug("Retrieving all events matching the given criteria")
        url = self._server_url + "/GET/events"

        filter_data = {
            "doCount": count_only,
            "startRecord": start_record,
            "nbRecordToFetch": records_limit,
            "activityIds": activities,
            "from": format_date(from_date) if from_date else from_date,
            "to": format_date(to_date) if to_date else to_date,
            "tagNames": tags,
        }
        filter_data = {k: v for k, v in filter_data.items() if v is not None}

        response = self._make_authd_request(
            'get', url,
            params={'filter': json.dumps(filter_data)},
        )
        logger.debug("GET/events request response: %d", response.status_code)
        return response

    # WARNING: still broken
    def delete_event(self, event_id: int) -> requests.Response:
        """
        Delete event with the specific event_id

        :param event_id:
        :return: request with the response
        """
        logger.debug("Deleting event %d", event_id)

        url = self._server_url + f"/DELETE/event/{event_id}"
        response = self._make_authd_request('delete', url)
        logger.debug("Request response: %d", response.status_code)
        return response

    def get_logbooks(self) -> requests.Response:
        """
        Get list of all logbooks

        :return: request with the response
        """
        logger.debug("Retrieving logbook list...")
        url = self._server_url + "/GET/logbooks"
        response = self._make_authd_request('get', url)
        logger.debug("Request response: %d", response.status_code)
        return response

    def get_tags(
            self,
            *,
            count_only: bool = False,
            records_limit: int = 1000,
            start_record: int = 0,
    ) -> requests.Response:
        """
        Get a list of tags.

        :return: request with the response
        """
        assert records_limit <= 1000 and start_record >= 0

        url = self._server_url + "/GET/tags/"
        parameters: typing.Dict[str, typing.Union[int, bool]] = {
            "doCount": count_only,
        }
        if not count_only:
            parameters.update(
                {
                    "startRecord": start_record,
                    "nbRecordToFetch": records_limit,
                },
            )

        response = self._make_authd_request(
            'get', url,
            params={'filter': json.dumps(parameters)},
        )
        return response
