"""Specific exceptions raised by pylogbook.

"""
import requests


class LogbookError(Exception):
    """Raised when any issues encountered during interaction with the LogbookServiceClient
    server.

    """
    def __init__(self, message: str, response: requests.Response) -> None:
        super().__init__(message)
        self._response = response

    @classmethod
    def raise_for_status(cls, response: requests.Response) -> None:
        if response.status_code == 401:
            raise AuthenticationError(
                'Logbook service not authenticated',
                response,
            )
        elif 400 <= response.status_code < 600:
            raise LogbookError(
                f"Logbook service error (code { response.status_code }) encountered",
                response,
            )


class AuthenticationError(LogbookError):
    """
    Raised when a 401 error is returned by the eLogbook service.

    """
    pass
