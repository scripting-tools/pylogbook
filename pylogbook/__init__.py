"""An interface to the logbook service.
"""

# pylogbook.exceptions is a public API which is automatically imported.
from . import exceptions as exceptions  # noqa
from ._activity_names import NamedActivity as NamedActivity
from ._client import ActivitiesClient as ActivitiesClient
from ._client import Client as Client
from ._server_names import NamedServer as NamedServer
from ._version import version as __version__  # noqa

ActivitiesClient.__module__ = __name__
Client.__module__ = __name__
NamedActivity.__module__ = __name__
NamedServer.__module__ = __name__
