# PyLogbook - interfacing with the eLogbook service

A python interface to the BE-OP Logbook API.

## Documentation

At its simplest level, posting to an eLogbook instance involves instantiating a Client, and posting an event to the client:

    import pylogbook
    cli = pylogbook.Client()
    event = cli.add_event("My event message", activities=pylogbook.NamedActivity.LHC)

There are a few things that you might choose to do from this point.
For full details of the API please see the documentation at
https://acc-py.web.cern.ch/gitlab/scripting-tools/pylogbook/.
